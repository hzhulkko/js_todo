# Testing a simple JS app with Robot Framework

This repository consists of a simple todo list application build with React, Node.js and Webpack and automated Robot Framework test cases.

The application is running online here: https://desolate-fjord-46587.herokuapp.com/

## Build, run and test the application locally from command line

To build and run the application, execute:

	npm start

Webpack bundles the source code in src/client/todo.js with dependencies and styles and generates an output in dist/main.js. The application will run in port 8080.

To run Robot Framework tests locally, execute:

	robot --variable PAGE:http://localhost:8080 --variable BROWSER firefox todo.robot


## Build, run and test the application using Docker

1. Go to project root directory

2. Build the application image:
    
    docker build -t todo/app .

3. Create a docker network:

    docker network create testnet

4. Run the application and connect it to the network:
    
    docker run --name todo-app --net testnet -u node  todo/app

5. Go to test directory (cd test)

6. Build the test image:

    docker build -t robotfw .

7. Run the tests in the same network:

    docker run --rm --net testnet robotfw

