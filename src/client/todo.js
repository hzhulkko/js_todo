import React from "react";
import ReactDOM from "react-dom";
import "../css/bootstrap.min.css";

function formatTime(ms) {
    let min = 0 | (ms/1000/60);
    let sec = 0 | (ms/1000) % 60;
    if (sec == 60) {
        min += 1;
        sec = 0;
    }
    return (min < 10 ? "0" + min : min) + ":" + (sec < 10 ? "0" + sec : sec);
}

class ToDoContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: [], 
            counter : 1,
            completed: 0}
        this.addToDo = this.addToDo.bind(this);
        this.completeToDo = this.completeToDo.bind(this);
    }

    addToDo(todo) { 
        const todoList = this.state.todos;
        todoList.push({"id": this.state.counter, "description": todo});
        this.setState({todos:todoList, counter : this.state.counter + 1});

    }

    completeToDo() {
        this.setState({completed: this.state.completed + 1});
    }

    render() {
        return (
            <div className="container w-50">
            <ToDoForm onSubmit={this.addToDo}/>
            <ToDoList todos={this.state.todos} onComplete={this.completeToDo}/>
            <ToDoStats total={this.state.todos.length} complete={this.state.completed}/>
            </div>
        );  
    }  
}

class ToDoForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        const todo = this.state.value;
        this.props.onSubmit(todo);
        this.setState({value: ""});
        event.preventDefault();
    }

    render() {
        return (
            <form className="form-inline" onSubmit={this.handleSubmit}>
            <label for="todoForm" className="mr-2">Thing to do:</label>
            <input className="flex-grow-1" id="todoForm" type="text" value={this.state.value} onChange={this.handleChange} />
            <input className="ml-2 btn btn-dark" type="submit" value="Add to list" />
            </form>
        );
    }
}


class ToDoList extends React.Component { 
    renderList() {
        const todos = this.props.todos;
        const complete = this.props.onComplete;
        var todoList = todos.map(function(todo){
            return <ToDo desc={todo.description} id={todo.id} onComplete={complete}/>
        })
        return todoList;
    }
    render() {
        var taskList = this.renderList();
        return (
            <table className="table table-hover mt-3">
            <tbody>
            {taskList}
            </tbody>
            </table>);
    }
}

class ToDo extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        const timerValue = Date.now();
        this.state = {done: false, timer: timerValue}
    }

    handleClick(event) {
        if (!this.state.done) {
            const startTime = this.state.timer;
            const stopTime = Date.now();
            this.setState({done: true, timer: (stopTime- startTime) });
            this.props.onComplete();
        }
        event.preventDefaults();
    }
    render() {
        const description = this.props.desc || "Hello";
        const id = this.props.id;
        const classname = (this.state.done) ? "table-success": "table-light";
        const timerValue = (this.state.done) ? "Duration: " + formatTime(this.state.timer) : "Click to complete";
        return (
            <tr className={classname} id={id} onClick={this.handleClick}>
            <td className="text-left">{description}</td> 
            <td className="text-right">{timerValue}</td>
            </tr>)
    }
}

class ToDoStats extends React.Component {
    render() {
        return (<p>Completed: <span>{this.props.complete} / {this.props.total}</span></p>)
    }
}

ReactDOM.render(
    <ToDoContainer/>,
    document.getElementById("main_container")
);
