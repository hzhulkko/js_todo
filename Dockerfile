FROM node:8

WORKDIR /opt/todo-app/

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 8080

CMD ["node", "src/server/server.js"]


