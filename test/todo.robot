*** Settings ***
Library     Selenium2Library
Suite Setup     Go to page

*** Comment ***
Suite Teardown  Close browser 

*** Variables ***
${PAGE}     http://todo-app:8080
${BROWSER}  headlessfirefox

*** Test Cases ***
Check that empty task adds hello
    When user clicks add to list
    Then task appears in the list   Hello

Check that tasks can be added
    When user writes a task     Test this thing
    When user clicks add to list
    Then task appears in the list   Test this thing

Check that tasks can be completed
    When user writes a task     Complete this task
    When user presses enter
    Then task appears in the list   Complete this task
    When user waits for     5 seconds
    When user clicks on the task   Complete this task
    Then correct time appears  00:05

*** Keywords ***
user writes a task
    [Arguments]     ${task}
    Input Text  id=todoForm    ${task}

user clicks add to list
    Click Button   Add to list

user presses enter
    Press Key   id=todoForm     \\13

task appears in the list
    [Arguments]     ${task} 
    Page Should Contain   ${task}

user waits for
    [Arguments]     ${time}
    Sleep   ${time}

user clicks on the task
    [Arguments]     ${task}
    Click Element   xpath://td[@class="text-left" and contains(text(), "${task}")]


correct time appears
    [Arguments]     ${timestring}
    Page Should Contain Element     xpath://td[@class="text-right" and contains(text(), "Duration: ${timestring}")]

Go to page
    Open Browser    ${PAGE}  ${BROWSER}

Close browser
    Close All Browsers
